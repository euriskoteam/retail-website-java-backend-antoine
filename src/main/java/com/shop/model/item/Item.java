package com.shop.model.item;

/**
 * Item Class
 * 
 * @author A.GHOSN
 */
public class Item {

	private String id;
	private String name;
	private ItemType type;
	private Double price;

	public Item(String id, String name, ItemType type, Double price) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.price = price;
	}

	/**
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return type
	 */
	public ItemType getType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 */
	public void setType(ItemType type) {
		this.type = type;
	}

	/**
	 * 
	 * @return price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * 
	 * @param price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * 
	 * @return id
	 */
	public String getId() {
		return id;
	}

}
