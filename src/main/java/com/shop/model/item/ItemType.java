package com.shop.model.item;

/**
 *
 * @author A.GHOSN
 */
public enum ItemType {
	GROCERY, ELECTRONICS, CLOTHES
}
