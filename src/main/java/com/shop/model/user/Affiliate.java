package com.shop.model.user;

import java.util.Date;

/**
 * Affiliate Class
 * 
 * @author A.GHOSN
 *
 */
public class Affiliate extends User {

	public Affiliate(String userId, String name, Date creationDate) {
		super(userId, name, creationDate);
		this.discountPercentage = 10;
	}

}
