package com.shop.model.user;

import java.util.Date;

import com.shop.utils.Utils;

/**
 * Affiliate Class
 * 
 * @author A.GHOSN
 *
 */
public class Customer extends User {

	public Customer(String userId, String name, Date creationDate) {
		super(userId, name, creationDate);
	}

	@Override
	public int getDiscountPercentage() {
		long diffYears = Utils.getDiffYears(this.getCreationDate(), new Date());
		if (diffYears > 2) {
			return 5;
		}
		else {
			return discountPercentage;
		}
	}
}
