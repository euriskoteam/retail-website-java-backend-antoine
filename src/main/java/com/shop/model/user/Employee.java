package com.shop.model.user;

import java.util.Date;

/**
 * Employee Class
 * 
 * @author A.GHOSN
 *
 */
public class Employee extends User {

	public Employee(String userId, String name, Date creationDate) {
		super(userId, name, creationDate);
		this.discountPercentage = 30;
	}

}
