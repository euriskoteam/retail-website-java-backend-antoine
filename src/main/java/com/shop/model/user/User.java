package com.shop.model.user;

import java.util.Date;

/**
 * User Class
 * 
 * @author A.GHOSN
 */
public class User {

	private String userId;
	private String name;
	protected int discountPercentage;
	private Date creationDate;

	public User(String userId, String name, Date creationDate) {
		this.discountPercentage = 0;
		this.userId = userId;
		this.name = name;
		this.creationDate = creationDate;
	}

	/**
	 * 
	 * @return userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * 
	 * @param userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * 
	 * @param creationDate
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(int discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

}
