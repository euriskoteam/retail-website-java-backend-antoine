package com.shop.model.shoppingCart;

import java.util.List;

import com.shop.model.item.Item;
import com.shop.model.user.User;

/**
 * Cart class
 * 
 * @author A.GHOSN
 */
public class Cart {

	private User user;
	private List<Item> items;
	private Double price;
	private Double finalPrice;

	/**
	 * 
	 * @param user
	 */
	public Cart(User user, List<Item> items){
		this.user = user;
		this.items = items;
	}

	/**
	 * 
	 * @return
	 */
	public User getUser() {
		return user;
	}

	/**
	 * 
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * 
	 * @return
	 */
	public List<Item> getItems() {
		return items;
	}

	/**
	 * 
	 * @param items
	 */
	public void setItems(List<Item> items) {
		this.items = items;
	}

	/**
	 * 
	 * @return price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * 
	 * @param price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * 
	 * @return finalPrice
	 */
	public Double getFinalPrice() {
		return finalPrice;
	}

	/**
	 * 
	 * @param finalPrice
	 */
	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}
}
