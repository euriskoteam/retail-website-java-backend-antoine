package com.shop.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * Utility class
 * 
 * @author A.GHOSN
 */

public final class Utils {

	private Utils() {

	}

	/**
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public final static long getDiffYears(Date from, Date to) {
		LocalDateTime fromDateTime = LocalDateTime.ofInstant(from.toInstant(), ZoneId.systemDefault());
		LocalDateTime toDateTime = LocalDateTime.ofInstant(to.toInstant(), ZoneId.systemDefault());
		return fromDateTime.until(toDateTime, ChronoUnit.YEARS);
	}

}
