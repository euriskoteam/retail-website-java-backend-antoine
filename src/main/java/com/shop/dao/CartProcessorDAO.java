package com.shop.dao;

import com.shop.model.item.Item;
import com.shop.model.item.ItemType;
import com.shop.model.shoppingCart.Cart;

/**
 * Responsible for processing cart payment price
 * 
 * @author A.GHOSN
 */
public class CartProcessorDAO {

	private static final int DISCOUNT_DIVISOR = 100;
	private static final int DISCOUNT_PER_DIVISOR = 5;

	/**
	 * Calculate total price cart (discounted price & total price without discount)
	 * 
	 * @param cart
	 */
	public Cart calculateCartTotal(Cart cart) {
		int discountPercentage = cart.getUser().getDiscountPercentage();
		double total = 0;
		double totalToDiscount = 0;
		double totalWithoutDiscount = 0;
		double itemPrice = 0;

		for (Item item : cart.getItems()) {
			itemPrice = item.getPrice();
			total += itemPrice;

			if (!ItemType.GROCERY.equals(item.getType())) {
				totalToDiscount += itemPrice;
			} else {
				totalWithoutDiscount += itemPrice;
			}
		}

		totalToDiscount = totalToDiscount - ((totalToDiscount * discountPercentage) / 100);
		totalToDiscount = totalToDiscount + totalWithoutDiscount;

		int nbOfDivisors = (int) (totalToDiscount / DISCOUNT_DIVISOR);
		totalToDiscount = totalToDiscount - (nbOfDivisors * DISCOUNT_PER_DIVISOR);

		cart.setFinalPrice(totalToDiscount);
		cart.setPrice(total);
		return cart;
	}
}
