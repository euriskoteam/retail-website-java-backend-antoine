package com.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.shop.model.shoppingCart.Cart;
import com.shop.service.ICartService;

@RestController
public class CartController {

	private final ICartService cartService;

	@Autowired
	public CartController(ICartService cartService) {
		this.cartService = cartService;
	}

	/**
	 * Post method that calculates total price
	 * 
	 * @param userId
	 * @param cart
	 * @return
	 */
	@RequestMapping(value = "/shop", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public double calculateCartTotal(@RequestBody Cart cart) {
		Cart treatedCart = cartService.calculateCartTotal(cart);
		return treatedCart.getFinalPrice();
	}
}
