package com.shop.service;

import org.springframework.stereotype.Service;

import com.shop.dao.CartProcessorDAO;
import com.shop.model.shoppingCart.Cart;

/**
 * 
 * @author A.GHOSN
 *
 */
@Service
public class CartServiceImpl implements ICartService {

	@Override
	public Cart calculateCartTotal(Cart cart) {
		CartProcessorDAO cartProcessor = new CartProcessorDAO();
		return cartProcessor.calculateCartTotal(cart);
	}
}
