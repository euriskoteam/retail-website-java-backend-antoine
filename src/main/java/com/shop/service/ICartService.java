package com.shop.service;

import com.shop.model.shoppingCart.Cart;

/**
 * Cart service interface
 * 
 * @author A.GHOSN
 *
 */
public interface ICartService {

	Cart calculateCartTotal(Cart cart);
}
