package com.retail.shoppingCart;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.shop.dao.CartProcessorDAO;
import com.shop.model.item.Item;
import com.shop.model.item.ItemType;
import com.shop.model.shoppingCart.Cart;
import com.shop.model.user.Customer;
import com.shop.model.user.Employee;
import com.shop.model.user.User;

public class CartProcessorTest {

	private CartProcessorDAO cartProcessorDAO;

	public CartProcessorTest() {
		this.cartProcessorDAO = new CartProcessorDAO();
	}

	@Test
	public void validateUserEmployee() {
		Date currentDate = new Date();
		User user = new Employee("1", "AHMAD", currentDate);

		Item item1 = new Item("1", "APPLE", ItemType.GROCERY, 50.0);
		Item item2 = new Item("2", "TV", ItemType.ELECTRONICS, 450.0);
		List<Item> items = new ArrayList<Item>();
		items.add(item1);
		items.add(item2);

		Cart cart = new Cart(user, items);
		cart = cartProcessorDAO.calculateCartTotal(cart);

		assertTrue(cart.getFinalPrice().equals(350.0));
	}

	@Test
	public void validateMoreThan2YearsCustomerDiscount() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -3);

		User user = new Customer("1", "OMAR", cal.getTime());

		Item item1 = new Item("1", "grocery", ItemType.GROCERY, 50.0);
		Item item2 = new Item("2", "TV", ItemType.ELECTRONICS, 450.0);
		List<Item> items = new ArrayList<Item>();
		items.add(item1);
		items.add(item2);

		Cart cart = new Cart(user, items);
		cart = cartProcessorDAO.calculateCartTotal(cart);

		assertTrue(cart.getFinalPrice().equals(457.5));
	}

	@Test
	public void validateCustomerDiscount() {
		Calendar cal = Calendar.getInstance();
		User user = new Customer("1", "Antoine", cal.getTime());

		Item item1 = new Item("1", "Cheese", ItemType.GROCERY, 50.0);
		Item item2 = new Item("2", "TV", ItemType.ELECTRONICS, 450.0);
		List<Item> items = new ArrayList<Item>();
		items.add(item1);
		items.add(item2);

		Cart cart = new Cart(user, items);
		cart = cartProcessorDAO.calculateCartTotal(cart);

		assertTrue(cart.getFinalPrice().equals(475.0));
	}

}
