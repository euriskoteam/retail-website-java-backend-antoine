This README file is responsible for explaining:
* The business rules behind the retail website.
* The process of running the code and the test.
* The coverage reports generation.

# Business Rules
1. If the user is an employee of the store, he gets a 30% discount
2. If the user is an affiliate of the store, he gets a 10% discount
3. If the user has been a customer for over 2 years, he gets a 5% discount.
4. For every $100 on the bill, there would be a $ 5 discount (e.g. for $ 990, you get $ 45
as a discount).
5. The percentage based discounts do not apply on groceries.
6. A user can get only one of the percentage based discounts on a bill.

# Run the Code and Test

Run the code by calling the following command
```bash
   mvn install
```
To run the unit tests run the following command

```bash
   mvn test
```

# Swagger Report
host:port/swagger-ui.html#/

# Coverage Reports

## SonarLint

Follow the steps below in order to install and run the **SonarLint** (IDE extension) on Eclipse IDE in order to detect quality issues of the code.

1. Open Eclipse.
2. Go to **Help >> Install New Software** from the menu.
3. Fill in the **Work With** field with the following line in order to install the market place:

*EPP Marketplace Client for Eclipse Photon - http://download.eclipse.org/mpc/photon*

4. Click on Next.
5. Click o Finish.
6. Go to **Help** from the menu.
7. Click on **Eclipse Marketplace**
8. Search for **sonarlint** 
9. Install **sonarlint**
10. Right click on the project root from the project navigator.
11. Choose SonarLint. Click on Analyze.
12. The project quality report will be generated.

You can find the sonar report and UML as images under the project

# UML Diagram
![UML](UML.png)